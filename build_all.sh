#!/bin/bash

######################################################################
#  Build script for ubicom32 toolchain
#
######################################################################

# Source common variables and functions
. common.sh

# Source config for the build
. config.build

######################################################################
#
# main script starts here
#
echo ""
date
echo ""

echo "Removing $BLDDIR"
rm -rf $BLDDIR
echo "Removing $RELDIR"
rm -rf $RELDIR

# private variables
SET_DATE=
CLEAN_UP_DATE=

# Process command line options
process_cmd_options
KERNEL_SRC=$CONFIG_KERNEL_SRC
UCLIBC_SRC=$CONFIG_UCLIBC_SRC

mkdir -p $BLDDIR
mkdir -p $RELDIR

Status "Configuration Parameters"
Info "Sources from  = $SRCDIR"
Info "Build using   = $BLDDIR"
Info "Release to    = $RELDIR"
Info "uClibc dir    = $UCLIBC_SRC"
Info "Kernel source = $KERNEL_SRC"

# always use elf toolchain
CROSS_COMPILE=$RELDIR/bin/ubicom32-elf-

# Info Fixing broken cvs install scripts
find $SRCDIR -name install-sh | xargs chmod +x

if [ "$CONFIG_SET_DATE" == "y" ]; then
    SET_DATE=$(date +%Y%m%d)
    CLEAN_UP_DATE="$SRCDIR/gcc/gcc/DATESTAMP.build_all_sav"
    mv $SRCDIR/gcc/gcc/DATESTAMP $CLEAN_UP_DATE
    echo $SET_DATE
 > $SRCDIR/gcc/gcc/DATESTAMP
fi

[ -z $SET_DATE ] || \
Info "Date	= $SET_DATE"

export LD_LIBRARY_PATH="$RELDIR/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"

######################################################################
# Configure and build uClinux/uClibc if needed

if $CONFIG_BUILD_UCLINUX_UCLIBC || $CONFIG_BUILD_LINUX_UCLIBC; then

    if [ ! -e "$UCLIBC_SRC" ]; then
	error_and_exit "$UCLIBC_SRC not found";
    fi

    if [ ! -e "$KERNEL_SRC" ]; then
	error_and_exit "$KERNEL_SRC not found";
    fi

fi

######################################################################
#  Build ELF tool chain

if $CONFIG_BUILD_ELF_TOOLCHAIN; then
  cd $ROOTDIR
  $ROOTDIR/build_elf_toolchain.sh || exit 1
fi

######################################################################
#  Build Linux tool chain

if $CONFIG_BUILD_UCLINUX_TOOLCHAIN || $CONFIG_BUILD_LINUX_TOOLCHAIN; then
  cd $ROOTDIR
  $ROOTDIR/build_linux_uclibc_toolchain.sh || exit 1
fi

######################################################################
Status "BUILD COMPLETE"
clean_up
echo ""
date
echo ""
######################################################################
