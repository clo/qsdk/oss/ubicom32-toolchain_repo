#!/bin/bash 

######################################################################
#  Build script for ubicom32 toolchain (-elf target)
#
######################################################################

echo 
echo "Building Ubicom32 Libraries"
date
echo 

# Source common variables and functions
. common.sh

# Source config for the build
. config.build

######################################################################
#
# main script starts here
#

RELDIR=$RELDIR
BLDDIR=$BLDDIR

Status "Configuration Parameters"
Info "Sources from  = $SRCDIR"
Info "Build using   = $BLDDIR"
Info "Release to    = $RELDIR"

[ -z $SET_DATE ] || \
Info "Date	= $SET_DATE"

export LD_LIBRARY_PATH="$RELDIR/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
PATH=$RELDIR/bin:$PATH
Info "LD_LIB_PATH   = $LD_LIBRARY_PATH"
Info "PATH          = $PATH"

# set elf target!!
set_target elf

######################################################################

    Status "Build Runtime Libraries"
    
    PATH=$RELDIR/bin:$PATH
    configure_and_build newlib newlib $SRCDIR/newlib/configure \
        --prefix=$RELDIR \
        --target=$TARGET \
	--enable-multilib


######################################################################
echo 
Status "BUILD COMPLETE"
date
