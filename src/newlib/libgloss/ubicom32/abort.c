#include <stdlib.h>
#include "intsys.h"


void
abort(void)
{
  return INT_SYS (SYS_kill, 0, 0, 0);
}
